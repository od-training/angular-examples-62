import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { Video } from './dashboard/app-types';

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {

  constructor(private http: HttpClient) { }

  loadVideos(): Observable<Video[]> {
    return this.http
      .get<Video[]>('https://api.angularbootcamp.com/videos')
      .pipe(
        map(allVideos => allVideos.slice(0, 3))
      )
    ;
  }

}
