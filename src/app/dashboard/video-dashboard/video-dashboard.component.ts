import { Component, OnInit } from '@angular/core';
import { Video } from '../app-types';
import { VideoDataService } from '../../video-data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.scss']
})
export class VideoDashboardComponent implements OnInit {

  videos$: Observable<Video[]>;

  selectedVideo: string | undefined;

  constructor(svc: VideoDataService) {
    this.videos$ = svc.loadVideos();
  }

  ngOnInit(): void {
  }

  onVideoSelected(id: string) {
    this.selectedVideo = id;
  }

}
